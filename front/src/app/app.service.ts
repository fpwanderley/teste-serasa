import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Debt, TotalDebt } from './debt';

@Injectable()
export class DebtService {

    private debtsUrl = 'https://ancient-coast-61469.herokuapp.com/api/v1/debts?consumer=';

    constructor(private http: Http) {}

    getDebts(cpf: string): Promise<Debt[]> {

        if(!cpf) {
            cpf = '-1';
        }

        return this.http
        .get(this.debtsUrl + cpf)
        .toPromise()
        .then(response => response.json() as Debt[])
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error Ocorred', error);
        return Promise.reject(error.message || error);
    }
}

@Injectable()
export class TotalDebtService {

    private debtsUrl = 'https://ancient-coast-61469.herokuapp.com/api/v1/get_total_debts?consumer_ids=';

    constructor(private http: Http) {}

    getTotalDebts(cpf: string): Promise<TotalDebt> {
        
        if(!cpf) {
            cpf = '-1';
        }

        return this.http
        .get(this.debtsUrl + cpf)
        .toPromise()
        .then(response => response.json() as TotalDebt)
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error Ocorred', error);
        return Promise.reject(error.message || error);
    }
}
