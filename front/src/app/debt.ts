export class Debt {
    id: number;
    description: string;
    paid: boolean;
    value: number;
}

export class TotalDebt {
    name: string;
    total_debt: number;
}