import { Component } from '@angular/core';
import { DebtService, TotalDebtService } from './app.service';
import { Debt, TotalDebt } from './debt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DebtService, TotalDebtService]
})

export class SerasaComponent{
    app_title = 'Serasa Consumidores';
    debts: Debt[];
    total_debt: undefined;
    
    consumer: Consumidor = {
        cpf: undefined,
    };

    constructor(private debtService: DebtService, private totalDebtService: TotalDebtService) {}

    getDebts(): void {
        this.debtService.getDebts(String(this.consumer.cpf)).then(debts => this.debts = debts);
    }

    getTotalDebts(): void {
        this.totalDebtService.getTotalDebts(String(this.consumer.cpf)).then(total_debt => this.total_debt = total_debt[0]);
    }

    onSearchDebt(consumer: Consumidor): void {
        console.log(this.debts);
        console.log(this.total_debt);
        this.getDebts();
        this.getTotalDebts();
    }
}

export class Consumidor {
    cpf: number;
}
