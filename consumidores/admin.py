# -*- coding: utf-8 -*-

import re

from django import forms
from django.contrib import admin
from .models import Consumer, Debt, User


class ConsumerForm(forms.ModelForm):

    class Meta:
        model = Consumer
        fields = ('name', 'cpf')
        widgets = {
            'cpf': forms.NumberInput(attrs={'min': 0,
                                            'max': 99999999999}
                                     )
        }

    def clean_cpf(self):
        """
            Validate whether the CPF has strictly 11 numbers.

        :return: Dict.
        """

        data = self.cleaned_data['cpf']

        if not bool(re.match('^[0-9]{11,11}$', data)):
            raise forms.ValidationError("CPF inválido.")

        return data


class ConsumerAdmin(admin.ModelAdmin):
    form = ConsumerForm
    list_display = ('name', 'cpf', 'get_total_debt')

    def get_total_debt(self, obj):
        return u'R$ {}'.format(obj.total_debt)
    get_total_debt.short_description = u'Dívida Total'

    class Meta:
        model = Consumer


class DebtForm(forms.ModelForm):

    class Meta:
        model = Debt
        fields = ('consumer', 'description', 'value', 'paid')


class DebtAdmin(admin.ModelAdmin):
    form = DebtForm
    list_display = ('consumer', 'description', 'value', 'paid')

    class Meta:
        model = Debt


class UserAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'username', 'is_superuser', 'is_admin')

    class Meta:
        model = User


admin.site.register(Consumer, ConsumerAdmin)
admin.site.register(Debt, DebtAdmin)
admin.site.register(User, UserAdmin)
