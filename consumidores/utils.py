

def prepare_string_to_id_list(raw_string):
    """
        Return a list to be used in Django's queries like (id__in=list_of_ids)

    - Each term in the list will be validated whether it is an Integer or not.
    - If a given list term is not valid, it will be simply EXCLUDED. The other
    valid terms will still be considered.

    :param raw_string: String like '1,2,3,4'
    :return: List like [1,2,3,4]
    """

    raw_ids = raw_string.split(',')

    prepared_ids = []
    for id in raw_ids:

        if id:
            try:
                prepared_ids.append(int(id))

            except ValueError:
                pass

    return prepared_ids



