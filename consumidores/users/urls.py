# -*- coding: utf-8 -*-

from django.conf.urls import url

from rest_framework import routers

from consumidores.users.endpoints import LoginViewSet, UserViewSet


user_router = routers.SimpleRouter(trailing_slash=False)
user_router.register(r'users', UserViewSet)

login_url = [
    url(r'^login', LoginViewSet.as_view(), name='login')
]
