# -*- coding: utf-8 -*-
"""Call Endpoints."""

from rest_framework import permissions, viewsets, status
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

from consumidores.models import User
from consumidores.users.serializers import UserSerializer
from consumidores.users.permissions import UserPermission


class LoginViewSet(APIView):

    def get(self, request, format=None):
        """
            Log the User in and return the User's token.
        """
        token, _ = Token.objects.get_or_create(user=request.user)
        data = {
            'user_email': request.user.email,
            'token': token.key,
            'id': request.user.id
        }
        return Response(data=data)


class UserViewSet(viewsets.ModelViewSet):
    """
        A simple ViewSet for viewing creating and editing Users.

    """

    # permission_classes = (AllowAny,)
    permission_classes = (UserPermission,)

    queryset = User.objects.all()
    serializer_class = UserSerializer
