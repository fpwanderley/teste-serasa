# -*- coding: utf-8 -*-

from django.conf.urls import url

from rest_framework import routers

from consumidores.debts.endpoints import DebtViewSet, TotalDebtViewSet


debts_router = routers.SimpleRouter(trailing_slash=False)
debts_router.register(r'debts', DebtViewSet, 'Debt')

total_debts_url = [
    url(r'^get_total_debt', TotalDebtViewSet.as_view(), name='get_total_debt')
]
