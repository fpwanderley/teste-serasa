# -*- coding: utf-8 -*-

from rest_framework import viewsets, status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response


from consumidores.models import Debt, Consumer
from consumidores.debts.serializers import DebtSerializer, TotalDebtsSerializer


class DebtViewSet(viewsets.ModelViewSet):
    """
        A simple ViewSet for viewing creating, editing, deleting and retrieving DEBTS.

    """

    permission_classes = (AllowAny,)

    serializer_class = DebtSerializer

    def get_queryset(self):
        consumer = self.request.query_params.get('consumer')
        debts = Debt.objects.all()

        if consumer:
            debts = debts.filter(consumer=consumer)

        return debts


class TotalDebtViewSet(APIView):
    """
        A simple view for retrieving the Consumer's Total Debt. \n
        Query Params: consumer_ids='1,2,3'.
    """

    permission_classes = (AllowAny,)

    # api/v1/get_total_debt?consumer_ids='1,2,3'
    def get(self, request):

        serializer = TotalDebtsSerializer(request.query_params)

        return Response(data=serializer.data,
                        status=status.HTTP_200_OK)
