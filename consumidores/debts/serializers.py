# -*- coding: utf-8 -*-

import re

from rest_framework import serializers

from consumidores.utils import prepare_string_to_id_list
from consumidores.models import Consumer, Debt


class DebtSerializer(serializers.ModelSerializer):

    consumer = serializers.PrimaryKeyRelatedField(many=False, queryset=Consumer.objects.all())

    class Meta:
        model = Debt
        fields = ('id', 'consumer', 'description', 'value', 'paid')


class TotalDebtsSerializer(serializers.BaseSerializer):

    def to_representation(self, obj):

        raw_consumer_ids = obj.get('consumer_ids', None)

        if raw_consumer_ids:
            prepared_consumer_ids = prepare_string_to_id_list(raw_consumer_ids)
            consumer_query = Consumer.objects.filter(id__in=prepared_consumer_ids)
        else:
            consumer_query = Consumer.objects.all()

        data = [
            {'id': consumer.id,
             'name': consumer.name,
             'total_debt': consumer.total_debt} for consumer in consumer_query
        ]

        return data
