# -*- coding: utf-8 -*-

import re

from rest_framework import serializers

from consumidores.models import Consumer


class ConsumerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Consumer
        fields = ('id', 'name', 'cpf')

    def validate_cpf(self, value):

        if not bool(re.match('^[0-9]{11,11}$', value)):
            raise serializers.ValidationError("CPF inválido.")

        return value
