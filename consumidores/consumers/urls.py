# -*- coding: utf-8 -*-

from rest_framework import routers

from consumidores.consumers.endpoints import ConsumerViewSet


consumers_router = routers.SimpleRouter(trailing_slash=False)
consumers_router.register(r'consumers', ConsumerViewSet, 'Consumer')
