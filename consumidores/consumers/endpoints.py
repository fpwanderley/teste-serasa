# -*- coding: utf-8 -*-

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny

from consumidores.models import Consumer
from consumidores.consumers.serializers import ConsumerSerializer


class ConsumerViewSet(viewsets.ModelViewSet):
    """
        A simple ViewSet for viewing creating, editing, deleting and retrieving CONSUMERS.

    """

    permission_classes = (IsAuthenticated,)

    queryset = Consumer.objects.all()
    serializer_class = ConsumerSerializer
