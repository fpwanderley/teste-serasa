# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import AbstractBaseUser as AuthUser
from django.contrib.auth.models import UserManager, PermissionsMixin


class User(AuthUser, PermissionsMixin):

    username = models.CharField(max_length=20, unique=True)
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=30)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'name']

    objects = UserManager()

    class Meta:
        verbose_name = u'Usuário'

    @property
    def get_short_name(self):
        return self.name


# Create your models here.
class Consumer(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, verbose_name=u'Nome')
    cpf = models.CharField(max_length=11, blank=False, null=False, verbose_name=u'CPF', unique=True)

    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=False)

    class Meta:
        verbose_name = u'Consumidor'
        verbose_name_plural = u'Consumidores'

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    @property
    def total_debt(self):
        """
            Return the Consumer's total debt.

        :return: Float.
        """

        return float(self.debts.filter(paid=False).aggregate(sum=Sum('value'))['sum'] or 0)


class Debt(models.Model):
    consumer = models.ForeignKey(Consumer, blank=False, null=False, verbose_name=u'Consumidor', related_name=u'debts')
    description = models.CharField(max_length=50, blank=False, null=False, default='', verbose_name=u'Descrição')
    value = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=u'Valor')
    paid = models.BooleanField(blank=False, null=False, default=False)

    created = models.DateTimeField(auto_now_add=True, auto_now=False, null=False)

    class Meta:
        verbose_name = u'Dívida'
        verbose_name_plural = u'Dívidas'

    def __repr__(self):
        return u'{consumer} - {description}'.format(consumer=self.consumer,
                                                    description=self.description or u'<Sem descrição>')

    def __str__(self):
        return self.__repr__()
