#!/usr/bin/env bash

# Script for Setting the Database up.

export PGPASSWORD=root
dropdb serasa -h db_container -p 5432 -U postgres
createdb serasa -h db_container -p 5432 -U postgres

# Deleting all migrations
ls -d1 /serasa/consumidores/migrations/* | grep -v '__' | xargs -d "\n" rm

python3.6 /serasa/manage.py makemigrations
python3.6 /serasa/manage.py migrate

python3.6 /serasa/manage.py createsuperuser
