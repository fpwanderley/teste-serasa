#!/usr/bin/env bash

# ##############################################################
echo "Updating Python Packages"
python3.6 -m pip install -r /serasa/requirements.txt

echo "Updating serasa Database"
source /serasa/setup_db.sh

echo "Starting Django Development Server"
python3.6 /serasa/manage.py runserver 0.0.0.0:8000
