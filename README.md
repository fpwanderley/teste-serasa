# Serasa Test
####  Requirements:
1.1 Docker 17.06 CE.

####  Setup Steps:
Download and create the needed DockerImages:
##### If you are in Ubuntu 14.04, run:
> source setup.sh

##### If you are in NOT in Ubuntu 14.04, run:
Download Postgres from Docker Hub:
> docker pull postgres

Build the application Docker images:
> source build_images.sh -i dev -v 1.0

> source build_images.sh -i envs -v 1.0

Database credentials (PostgreSQL/PgAdmin3):

- HOST: localhost
- PORT: 5432
- USERNAME: postgres
- PASSWORD: root

Running the application DockerImage:
> source run_images.sh -i envs -v 1.0 -c /bin/bash

##### Once you are in the application container:

Set the Angular up:
> npm install -g @angular/cli (It may be necessary to stop the terminal if it stuck.)
> npm install -g typescript

Serve Angular:
> cd /serasa/front && ng serve -H 0.0.0.0

> Acesso: http://localhost:4200/

Install Django application requirement:
> pip_install_requirements

Set the Database up and create a SuperUser:
> setup_db

Start the development server:
> runserver

Application URLs:

> Heroku URL: https://ancient-coast-61469.herokuapp.com

> Heroku Credentials: serasa//consumidores

> ADMIN: /admin

> LOGIN: /api/v1/login

> SWAGGER: /#/
