FROM serasa-dev:1.0
MAINTAINER fpwanderley@gmail.com
# ##############################################################
ENV EXTRA_PYTHON_PACKAGES Django==1.11.4 ipython==6.1.0 psycopg2 django-extensions==1.9.0 djangorestframework==3.7.1
ENV EXTRA_NPM_PACKAGES @angular/cli typescript angular-in-memory-web-api

# # ##############################################################
# # Installing Python (BackEnd) and NPM (FrontEnd) Packages
# # ##############################################################
RUN python3.6 -m pip install $EXTRA_PYTHON_PACKAGES
#RUN npm install -g $EXTRA_NPM_PACKAGES  // Ao instalar o Angular, o terminal entra em loop.

# # ##############################################################
# # Useful Commands
# # ##############################################################
RUN echo 'alias requirements="/serasa/requirements.txt"' >> ~/.bashrc
RUN echo 'alias pip_freeze="python3.6 -m pip freeze"' >> ~/.bashrc
RUN echo 'alias update_python_libs="pip_freeze > /serasa/requirements.txt"' >> ~/.bashrc
RUN echo 'alias pip_install="python3.6 -m pip install"' >> ~/.bashrc
RUN echo 'alias pip_install_requirements="python3.6 -m pip install -r /serasa/requirements.txt"' >> ~/.bashrc
RUN echo 'alias manage="python /serasa/manage.py "' >> ~/.bashrc
RUN echo 'alias runserver="manage runserver 0.0.0.0:8000"' >> ~/.bashrc
RUN echo 'alias createsuperuser="manage createsuperuser --username admin_user --email serasa@serasa.com"' >> ~/.bashrc
RUN echo 'alias setup_db="source /serasa/setup_db.sh"' >> ~/.bashrc
RUN echo 'alias start_server="source /serasa/start_server.sh"' >> ~/.bashrc
RUN echo 'alias install_angular_cli="sudo npm install -g @angular/cli"' >> ~/.bashrc
RUN echo 'alias run_angular="cd /serasa/front && ng serve -H 0.0.0.0"' >> ~/.bashrc

# # #################################################################################
# # Install all Python Application's Requirements and Turn the Development WebServer.
# # #################################################################################
CMD chmod +x /serasa/start_server.sh && \
    /serasa/start_server.sh  && \
    /bin/bash
